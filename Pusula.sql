-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 08 May 2020, 11:32:10
-- Sunucu sürümü: 10.4.11-MariaDB
-- PHP Sürümü: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `pusula`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `icerik`
--

CREATE TABLE `icerik` (
  `icerikid` int(11) NOT NULL,
  `kategoriname` longtext NOT NULL,
  `userName` varchar(45) NOT NULL,
  `icerikbaslik` varchar(30) NOT NULL,
  `icerikyazi` longtext NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Tablo döküm verisi `icerik`
--

INSERT INTO `icerik` (`icerikid`, `kategoriname`, `userName`, `icerikbaslik`, `icerikyazi`, `date`) VALUES
(102, 'Müzik', 'user', 'baslik', 'icerik', '2020-05-08');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kategoriler`
--

CREATE TABLE `kategoriler` (
  `id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Tablo döküm verisi `kategoriler`
--

INSERT INTO `kategoriler` (`id`, `name`) VALUES
(12, 'Günlük'),
(3, 'Mühendislik'),
(4, 'Müzik'),
(5, 'Programlama'),
(6, 'Resim'),
(7, 'Siyaset'),
(8, 'Spor'),
(9, 'Teknoloji');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `tema`
--

CREATE TABLE `tema` (
  `userName` varchar(45) COLLATE utf8_turkish_ci NOT NULL,
  `tema` varchar(20) COLLATE utf8_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `tema`
--

INSERT INTO `tema` (`userName`, `tema`) VALUES
('mod', 'themeFruits'),
('user', 'themeFruits');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user-kategoriler`
--

CREATE TABLE `user-kategoriler` (
  `id` int(11) NOT NULL,
  `userName` varchar(25) COLLATE utf8_turkish_ci NOT NULL,
  `kategoriName` varchar(45) COLLATE utf8_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `mail` varchar(45) COLLATE utf8_turkish_ci NOT NULL,
  `name` varchar(45) COLLATE utf8_turkish_ci NOT NULL,
  `password` text COLLATE utf8_turkish_ci NOT NULL,
  `blogName` varchar(25) COLLATE utf8_turkish_ci DEFAULT NULL,
  `about` varchar(240) COLLATE utf8_turkish_ci DEFAULT NULL,
  `yetki` varchar(25) COLLATE utf8_turkish_ci NOT NULL,
  `tarih` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`mail`, `name`, `password`, `blogName`, `about`, `yetki`, `tarih`) VALUES
('onat', 'admin', 'admin', 'f', 'f', 'admin', '2020-04-09'),
('mod', 'mod', 'mod', NULL, NULL, 'mod', '2020-05-07'),
('user', 'user', 'user', 'userin blogu', 'user user user', 'user', '2020-05-07');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `icerik`
--
ALTER TABLE `icerik`
  ADD PRIMARY KEY (`icerikid`);

--
-- Tablo için indeksler `kategoriler`
--
ALTER TABLE `kategoriler`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `name_2` (`name`);

--
-- Tablo için indeksler `tema`
--
ALTER TABLE `tema`
  ADD PRIMARY KEY (`userName`),
  ADD UNIQUE KEY `userName_2` (`userName`),
  ADD KEY `userName` (`userName`);

--
-- Tablo için indeksler `user-kategoriler`
--
ALTER TABLE `user-kategoriler`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategoriName` (`kategoriName`),
  ADD KEY `userName` (`userName`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `mail` (`mail`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `icerik`
--
ALTER TABLE `icerik`
  MODIFY `icerikid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- Tablo için AUTO_INCREMENT değeri `kategoriler`
--
ALTER TABLE `kategoriler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Tablo için AUTO_INCREMENT değeri `user-kategoriler`
--
ALTER TABLE `user-kategoriler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `tema`
--
ALTER TABLE `tema`
  ADD CONSTRAINT `tema_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `users` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `user-kategoriler`
--
ALTER TABLE `user-kategoriler`
  ADD CONSTRAINT `user-kategoriler_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `users` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
