package entities;


public class Contents {
	private String publishDate;
	private String header;
	private String content;
	private String category;

	public Contents(String publishDate, String header, String category, String content) {
		this.publishDate = publishDate;
		this.header = header;
		this.content = content;
		this.category = category;
	}
	
	

	public Contents(String publishDate, String header, String content) {
		super();
		this.publishDate = publishDate;
		this.header = header;
		this.content = content;
	}



	public Contents() {

	}

	public String getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

	public String getHeader() {
		return header;
	}

	public void settHeader(String header) {
		this.header = header;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	
}