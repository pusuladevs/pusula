package entities;

import java.sql.Date;
import java.util.ArrayList;


public class User {
	private String userName;
	private String about;
	private String blogName;
	private Date date;
	private ArrayList<Contents> contentList = new ArrayList<Contents>();
	

	public User(String userName, String about, String blogName) {
		this.userName = userName;
		this.about = about;
		this.blogName = blogName;
	}

	public User() {
	}

	public ArrayList<Contents> getContentList() {
		return contentList;
	}

	public void setContentList(ArrayList<Contents> contents) {
		this.contentList = contents;
	}

	public void addContentToList(Contents content) {
		this.contentList.add(content);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getBlogName() {
		return blogName;
	}

	public void setBlogName(String blogName) {
		this.blogName = blogName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}




}
