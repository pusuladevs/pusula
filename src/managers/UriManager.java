package managers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.faces.application.ResourceHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(filterName = "UriManager", urlPatterns = { "/blogger/*" })
public class UriManager implements Filter {
	

	public String[] list = {""};
	public String tema;
	public static String userName;
	
	public UriManager() {
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}
	
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			HttpServletRequest reqt = (HttpServletRequest) request;
			HttpServletResponse resp = (HttpServletResponse) response;
			HttpSession ses = reqt.getSession(false);

			String reqURI = reqt.getRequestURI();

			boolean resourceRequest = reqt.getRequestURI()
					.startsWith(reqt.getContextPath() + "/faces" + ResourceHandler.RESOURCE_IDENTIFIER);
			
			
			if (!resourceRequest || reqURI.indexOf("/blogger") > 0 || reqURI.indexOf("theme") > 0) {
				String array[] = reqURI.split("/");
				String lastIndex[] = array[array.length - 1].split("\\.");
				userName = lastIndex[0];
				
				
				try {
					if (!reqURI.equals(reqt.getContextPath() + "/loader.xhtml")) {
						if (!resourceRequest) { // Prevent restricted pages from being cached.
				            resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
				            resp.setHeader("Pragma", "no-cache"); // HTTP 1.0.
				            resp.setDateHeader("Expires", 0); // Proxies.
				        }
						resp.sendRedirect(reqt.getContextPath() + "/loader.xhtml");
					}
					else {
						if (!resourceRequest) { // Prevent restricted pages from being cached.
				            resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
				            resp.setHeader("Pragma", "no-cache"); // HTTP 1.0.
				            resp.setDateHeader("Expires", 0); // Proxies.
				        }
						chain.doFilter(reqt, resp);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			System.out.println("redirect error: " + e.getMessage());
		}
	}

	@Override
	public void destroy() {

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}