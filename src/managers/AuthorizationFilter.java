package managers;

import java.io.IOException;

import javax.faces.application.ResourceHandler;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(filterName = "AuthFilter", urlPatterns = { "*.xhtml" })
public class AuthorizationFilter implements Filter {

	public AuthorizationFilter() {
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		try {
			HttpServletRequest reqt = (HttpServletRequest) request;
			HttpServletResponse resp = (HttpServletResponse) response;
			HttpSession ses = reqt.getSession(false);

//			String loginURI = reqt.getContextPath() + "/loginpage.xhtml";
			String reqURI = reqt.getRequestURI();

//			boolean loginRequest = reqURI.startsWith(loginURI);
			boolean resourceRequest = reqt.getRequestURI()
				.startsWith(reqt.getContextPath() + "/faces" + ResourceHandler.RESOURCE_IDENTIFIER);
			boolean isLogged = ((reqt.getSession().getAttribute("auth") == null)
					|| reqt.getSession().getAttribute("auth").equals("")) ? false : true;

			if (isLogged) {
				if (ses.getAttribute("auth").equals("admin")) {
					if (reqURI.indexOf("mod") > 0 || reqURI.indexOf("user") > 0) {
						resp.sendRedirect(reqt.getContextPath() + "/authError.xhtml");
					} else {
						chain.doFilter(request, response);
					}
				}
				if (ses.getAttribute("auth").equals("mod")) {
					if (reqURI.indexOf("admin") > 0 || reqURI.indexOf("user") > 0) {
						resp.sendRedirect(reqt.getContextPath() + "/authError.xhtml");
					} else {
						chain.doFilter(request, response);
					}
				}
				if (ses.getAttribute("auth").equals("user")) {
					if (reqURI.indexOf("admin") > 0 || reqURI.indexOf("mod") > 0) {
						resp.sendRedirect(reqt.getContextPath() + "/authError.xhtml");
					} else {
						chain.doFilter(request, response);
					}
				}
				if (ses.getAttribute("auth") == null) {
					chain.doFilter(request, response);
				}
			} else {
				if (reqURI.indexOf("admin") > 0 || reqURI.indexOf("mod") > 0 || reqURI.indexOf("user") > 0) {
					if (!resourceRequest) { // Prevent restricted pages from being cached.
			            resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
			            resp.setHeader("Pragma", "no-cache"); // HTTP 1.0.
			            resp.setDateHeader("Expires", 0); // Proxies.
			        }
					resp.sendRedirect(reqt.getContextPath() + "/authError.xhtml");
				} else {
					chain.doFilter(request, response);
				}
			}
		} catch (Exception e) {
			System.out.println("redirect error: " + e.getMessage());
		}
	}

	@Override
	public void destroy() {

	}
}