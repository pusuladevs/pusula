package model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.*;
import org.apache.commons.io.FileUtils;


import java.io.*;

public class userDB {

	public String yetki, about, blogName;
	public static String name, password, mail;
	public static Integer id;
	public Date date;
	public String msg = "";
	public String uname = getName();

	public userDB(String uyekuladi, String uyesifre, String uyemail, String uyeyetki, Date date2) {
		name = uyekuladi;
		password = uyesifre;
		mail = uyemail;
		this.yetki = uyeyetki;
		this.date = date2;
	}
	
	public userDB(String about, String blogName) {
		this.blogName = blogName;
		this.about = about;
	}

	public userDB() {
		// TODO Auto-generated constructor stub
	}

	public void delUser() {
		String sql = "DELETE FROM users WHERE name = ?";

		try {
			Connection con = ConnectToDB.getConnect();

			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, getName());

			ps.execute();

			con.close();

		} catch (SQLException e) {
			System.out.println(e);
		}
	}

	public void addUser() {
		// INSERT INTO `users` (`iduser`, `mail`, `name`, `password`, `url`, `baslik`,
		// `hakkında`, `yetki`, `tarih`) VALUES
		// (NULL, 'ert', 'ert', 'ert', 'ert', 'ert', 'ert', 'user', '2020-04-09');
		String sql = "INSERT INTO users ( mail, name, password,  yetki, tarih) VALUES (? , ?, ?, ?, ?)";
		String sqlTema = "INSERT INTO `tema` ( `userName`, `tema`) VALUES ( ?, 'themeFruits')";
		try {
			Connection con = ConnectToDB.getConnect();

			PreparedStatement pr = con.prepareStatement(sql);

			pr.setString(1, getMail());
			pr.setString(2, getName());
			pr.setString(3, getPassword());
			pr.setString(4, getYetki());
			pr.setDate(5, getDate());

			pr.execute();

			PreparedStatement prTema = con.prepareStatement(sqlTema);

			prTema.setString(1, getName());

			prTema.execute();

			con.close();
			System.out.println("Add User and Theme Succes ");
			msg = "Kay�t Ba�ar�l�!";
			selectUrl(getName());
		} catch (SQLException e) {
			System.out.println("Kay�t Ba�ar�s�z :" + e);
			msg = "Hata" + e;
		}

	}

	public void selectUrl(String loginURLUser) {
		String pathToDefault = ((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext())
				.getRealPath("/themeGame.xhtml");
		String pathToNew = ((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext())
				.getRealPath("/" + getName() + ".xhtml");

		File templateFile = new File(pathToDefault);
		String htmlString;

		try {

			htmlString = FileUtils.readFileToString(templateFile);
//			String title = "New Pageeee";
//			String body = "asdfas";
//			String user =   "<f:attribute name="value" value="+  +" />" ;
//			htmlString = htmlString.replace("$title", title);
//			htmlString = htmlString.replace("$body", body);
//			htmlString = htmlString.replace("$user", user);
			File newHtmlFile = new File(pathToNew);
			FileUtils.writeStringToFile(newHtmlFile, htmlString);

			System.out.println(newHtmlFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean loginUser(String userName, String pass) {
		String sql = "SELECT * FROM users WHERE name = ? and password = ?";
		
		try {
			Connection con = ConnectToDB.getConnect();
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, userName);
			ps.setString(2, pass);

			ResultSet rs = ps.executeQuery();

			if (!rs.next()) {
				return false;
			} else {
				do {
					setName(rs.getString("name"));
					setYetki(rs.getString("yetki"));
					setMail(rs.getString("mail"));
					setPassword(rs.getString("password"));
				} while (rs.next());

				return true;
			}
		} catch (SQLException e) {
			return false;
		}
	}

	public void resetPass() {
		String sql = "SELECT * FROM users WHERE mail LIKE ?";
		try {
			Connection con = ConnectToDB.getConnect();

			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, getMail());

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				setPassword(rs.getString("password"));
			}

			con.close();

		} catch (SQLException e) {
			System.out.println(e);
		}

	}

	public void editUserName(String newName, String oldName) {
		// UPDATE `users` SET `name` = 'ase' WHERE `users`.`name` = 'on'
		String sql = "UPDATE `users` SET `name` = ? WHERE `users`.`name` = ?";
		try {
			Connection con = ConnectToDB.getConnect();

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, newName);
			ps.setString(2, oldName);

			ps.execute();

			con.close();
			setName(newName);
			System.out.println("Set Name Success");
		} catch (SQLException e) {
			System.out.println("Warning set name failled" + e);

		}

	}

	public void editUserPassword(String newPass) {
		String sql = "UPDATE `users` SET `password` = ? WHERE `users`.`name` = ?";
		try {
			Connection con = ConnectToDB.getConnect();

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, newPass);
			ps.setString(2, getName());

			ps.execute();

			con.close();

			System.out.println("Set Password Succes");
		} catch (SQLException e) {
			System.out.println("Warning Set Password Failled  " + e);

		}

	}

	public void userSetBlogName() {
		String sql = "UPDATE `users` SET `blogName` = ? WHERE `users`.`name` = ?";
		try {
			Connection con = ConnectToDB.getConnect();

			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, getBlogName());
			ps.setString(2, getName());

			ps.execute();

			con.close();

			System.out.println(" about and blogName is set succes");

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("set Tema failled " + e);
		}
	}

	public void userSetAbout() {
		String sql = "UPDATE `users` SET `about` = ? WHERE `users`.`name` = ?";
		try {
			Connection con = ConnectToDB.getConnect();

			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, getAbout());
			ps.setString(2, getName());

			ps.execute();

			con.close();

			System.out.println(" about and blogName is set succes");

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("set Tema failled " + e);
		}
	}

	ArrayList<userDB> tema = new ArrayList<userDB>();
	public List<userDB> editTema(String name) {
		String sql = "SELECT blogName , about FROM `users` WHERE `name` LIKE ? "; 
		try {
			Connection con = ConnectToDB.getConnect();
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, name);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				tema.add(new userDB(rs.getString("about"),rs.getString("blogName")));
			}
			
			return tema;
		}catch (SQLException e) {
			System.out.println("edit Tema is Fail.." + e);
			return null;
		}
	}
	public void deleteAccount(String name) {
		String sql = "DELETE FROM `users` WHERE `users`.`name` = ?";
		try {
			Connection con = ConnectToDB.getConnect();

			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, name);
			ps.execute();

			con.close();
			System.out.println("delete account syccse");
		} catch (SQLException e) {
			System.out.println("failled delete acooynr " + e);
		}
	}

	public void updateMail(String name, String mail) {
		// UPDATE `users` SET `name` = ? WHERE `users`.`name` = ?
		String sql = "UPDATE `users` SET `mail` = ? WHERE `users`.`name` = ?";

		try {
			Connection con = ConnectToDB.getConnect();

			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, mail);
			ps.setString(2, name);

			ps.execute();

			con.close();

			System.out.println("succes to update mail");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("update mail falled ");
		}

	}

	// Getters and Setters

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public static String getName() {
		return name;
	}

	public static void setName(String name) {
		userDB.name = name;
	}

	public static String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		userDB.password = password;
	}

	public static String getMail() {
		return mail;
	}

	public static void setMail(String mail) {
		userDB.mail = mail;
	}

	public String getYetki() {
		return yetki;
	}

	public void setYetki(String yetki) {
		this.yetki = yetki;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getBlogName() {
		return blogName;
	}

	public void setBlogName(String blogName) {
		this.blogName = blogName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}