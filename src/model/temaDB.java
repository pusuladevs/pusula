package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class temaDB {
	public  Integer id;
	public  String userName,temaName;
	
	public temaDB(Integer id, String userName,String temaName) {
		super();
		this.id = id;
		this.userName = userName;

		this.temaName = temaName;
	}

	public temaDB() {
		//cons
	}

	public String userTema(String name) {
		String sql = "SELECT tema FROM `tema` WHERE `userName` LIKE ?";
		String Tema = null;
		try {
			Connection con = ConnectToDB.getConnect();
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, name);
			
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				Tema = rs.getString("tema");
			}
			
			con.close();
			return Tema;
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return null;
		}
	}
	
	public void updateTema() {
		String sql = "UPDATE `tema` SET `tema` = ? WHERE `tema`.`userName` = ?";
		
		try {
			Connection con = ConnectToDB.getConnect();
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, getTemaName());
			ps.setString(2, getUserName());
			
			ps.execute();
			
			con.close();
			System.out.println("select tema succes");
		}catch (SQLException e) {
			System.out.println("failed select Tema " + e);
		}
		
	}
	
	// Getters and Setters
	
	public String getTemaName() {
		return temaName;
	}

	public void setTemaName(String temaName) {
		this.temaName = temaName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	

}
