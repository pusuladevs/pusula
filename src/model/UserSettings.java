package model;

public class UserSettings {
	static String loginUserName,loginUserPassword,loginUserMail;
	
	
	
	public UserSettings(String loginUserName, String loginUserPassword, String loginUserMail) {
		super();
		UserSettings.loginUserName = loginUserName;
		UserSettings.loginUserPassword = loginUserPassword;
		UserSettings.loginUserMail = loginUserMail;
	}


	userDB user = new userDB();
	public void updateUserName(String userName) {
		System.out.println(userName);
		user.editUserName(userName, UserSettings.loginUserName);
	}
	
	public void updateEmail(String email) {
		
		System.out.println("last mail : " + UserSettings.loginUserMail + "new mail :" +email +" name : " + userDB.getName());
		user.updateMail(userDB.getName(), email);
	}
	
	public void updateBlogName(String blogName) {
		System.out.println(blogName);
		user.setBlogName(blogName);
		user.userSetBlogName();
	}
	
	public void updateAbout(String about) {
		System.out.println(about);
		user.setAbout(about);
		user.userSetAbout();
	}
	
	public void updatePassword(String pass) {
		//TODO : password controller 
		System.out.println(pass);
		user.editUserPassword(pass);
	}
	
	public void updateVisibility(boolean visibility) {
		System.out.println("visibility:" + visibility);
	}
	
	public void delAccount() {
		System.out.println("delAccount userSettingsdb");
		user.delUser();
	}
	
	// içerik silinecek
	public void delBlog() {
		System.out.println("delBlog userSettingsdb");
	}
	
	
}
