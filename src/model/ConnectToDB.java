package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectToDB {
	
	 public static Connection getConnect() throws SQLException {
		 Connection con = null;
		 String url = "jdbc:mysql://localhost:3306/Pusula?useUnicode=true&useJDBCCompliantTimezoneShift"
		 		+ "=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		 
		 try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "");
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
		
//	        Connection con = DriverManager.getConnection(url,"root","");
	        return  con;
	    }
	 
}
