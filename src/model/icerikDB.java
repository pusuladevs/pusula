package model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.userDB;
public class icerikDB {
	                                                          
	public String icerikbaslik,kategoriname,icerikyazi,msg;                                                               
	public Date date;
	public String userName=userDB.getName();
	public userDB user = new userDB();                                                                                                                     
	
	public icerikDB(String userName,String icerikbaslik, String icerikyazi, String kategoriname, Date date) {
		super();
		this.userName = userName;
		this.icerikbaslik = icerikbaslik;
		this.icerikyazi = icerikyazi;
		this.kategoriname = kategoriname;
		this.date = date;
	}
	
	public icerikDB() {
		//cons
	}           
	
	public void addIcerik() {
		System.out.println("userName" + getUserName());
		String sql = "INSERT INTO icerik (icerikid, kategoriname,userName, icerikbaslik, icerikyazi,date) VALUES (NULL, ? ,?,?,?,?)";
		try {
			Connection con = ConnectToDB.getConnect();
			
			PreparedStatement pr = con.prepareStatement(sql);
			
			pr.setString(1,getKategoriname());
			pr.setString(2, getUserName());
			pr.setString(3,getIcerikbaslik());
			pr.setString(4,getIcerikyazi());
	        pr.setDate(5,getDate());
			
			pr.execute();                
			
			con.close();
			System.out.println("Icerik basariyla eklendi.");
			
			}catch(SQLException e) {
				System.out.println("Islem basarisiz!" + e);
				
				}
		
	}
	
	public void delIcerik() {
		
		String sql = "DELETE FROM `icerik` WHERE `icerik`.`icerikbaslik` = ?";
		
		try {
			Connection con = ConnectToDB.getConnect();
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, getIcerikbaslik());
			
			ps.execute();
			
			con.close();
			
		}catch (SQLException e){
			System.out.println(e);
		}
		
	}
	
	ArrayList<icerikDB> list = new ArrayList<icerikDB>();
	public List<icerikDB> getContent(String userName) {
		String sql ="SELECT kategoriname,icerikbaslik,icerikyazi,date FROM `icerik` WHERE `userName` = ?";
		
		try {

			
			Connection con = ConnectToDB.getConnect();
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, userName);
			
			ResultSet rs = ps.executeQuery();
			int index = 0;
			while(rs.next()) {
				list.add(new icerikDB(userName, rs.getString("icerikbaslik"),rs.getString("icerikyazi"),rs.getString("kategoriname"),rs.getDate("date")));
				System.out.println("index: " + index);
				index++;
			}
			con.close();
			return list;
		}catch(SQLException e) {
			System.out.println("get content failled " + e);
			return null;
		}
		}
	
	public ResultSet selectKategori(String userName ,String kategoriName) {
		String sql = "SELECT `icerikbaslik` FROM `icerik` WHERE `kategoriname` LIKE ? AND `userName` = ?";
		try {
			Connection con = ConnectToDB.getConnect();
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, kategoriName);
			ps.setString(2, userName);
			
			ResultSet rs = ps.executeQuery();
			
			System.out.println("select categories succes");
			return rs;
		}catch(SQLException e) {
			System.out.println("failled select Categories" + e);
			return null;
		}
	}
	
	//Getters and Setters
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public String getIcerikbaslik() {
		return icerikbaslik;
	}
	public void setIcerikbaslik(String icerikbaslik) {
		this.icerikbaslik = icerikbaslik;
	}
	public String getIcerikyazi() {
		return icerikyazi;
	}
	public void setIcerikyazi(String icerikyazi) {
		this.icerikyazi = icerikyazi;
	}
	public String getKategoriname() {
		return kategoriname;
	}
	public void setKategoriname(String checked) {
		this.kategoriname = checked;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
	


