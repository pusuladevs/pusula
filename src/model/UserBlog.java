package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.servlet.http.*;
import javax.faces.context.ExternalContext;
import entities.Contents;
import entities.User;

public class UserBlog {
	String usersName;
	SimpleDateFormat dateformatter = new SimpleDateFormat("dd/MM/yyyy");
	private ArrayList<User> users = new ArrayList<User>();
	public ArrayList<String> headers = new ArrayList<String>();
	private User user;
	
	public UserBlog() {}

	public class CategoriesWithHeaders {
		private ArrayList<String> headers;
		private String category;

		public CategoriesWithHeaders(String category, ArrayList<String> headers) {
			this.setCategory(category);
			this.setHeaders(headers);
		}

		public CategoriesWithHeaders() {
		}

		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public ArrayList<String> getHeaders() {
			return headers;
		}

		public void setHeaders(ArrayList<String> headers2) {
			this.headers = headers2;
		}
	}
	
	public User fetchUser(String userName) {
		User user = null;
		String sql = "SELECT blogName , about FROM `users` WHERE `name` LIKE ? "; 
		try {
			Connection con = ConnectToDB.getConnect();
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, userName);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				user = new User(userName ,rs.getString("about"), rs.getString("blogName"));
			}
			return user;
		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public ArrayList<String> fetchCategories(String userName) {
		ArrayList<String> categories = new ArrayList<String>();
		String sql = "SELECT `kategoriName` FROM `icerik` WHERE `userName` LIKE ? GROUP BY `kategoriName`";
		try {
			Connection con = ConnectToDB.getConnect();

			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, userName);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				categories.add(rs.getString("kategoriName"));
			}
			con.close();
			return categories;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return categories;
	}
	
	public CategoriesWithHeaders fetchCategoriesWithHeaders(String category, String userName) {

		CategoriesWithHeaders cwh = null;
		ArrayList<String> headers = new ArrayList<String>();

		String sql = "SELECT icerikbaslik FROM `icerik` WHERE `kategoriname` LIKE ? AND `userName` LIKE ?";
		try {
			Connection con = ConnectToDB.getConnect();

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, category);
			ps.setString(2, userName);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				headers.add(rs.getString("icerikbaslik"));
			}
			this.headers = headers;

			cwh = new CategoriesWithHeaders(category, headers);
			return cwh;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cwh;
	}
	
public Contents fetchContent(String header, String userName) {
		Contents contents = null;
		String sql = "SELECT icerikyazi,date FROM `icerik` WHERE `icerikbaslik` LIKE ? AND `userName` LIKE ?";
		try {
			Connection con = ConnectToDB.getConnect();

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, header);
			ps.setString(2, userName);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				contents = new Contents(dateformatter.format(rs.getDate("date")), header, rs.getString("icerikyazi"));
			}
			
			return contents;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contents;
	}
	

//	public ArrayList<String> fetchHeaders(String userName) {
//		String sql = "SELECT icerikbaslik FROM `icerik` WHERE `kategoriname` LIKE ? AND `userName` LIKE ?";
//		try {
//			Connection con = ConnectToDB.getConnect();
//			
//			PreparedStatement ps = con.prepareStatement(sql);
//			ps.setString(1, KategoriName);
//			ps.setString(2, Name);
//			
//			ResultSet rs = ps.executeQuery();
//			
//			while(rs.next()) {
//				System.out.println("icerikbaslik : " +rs.getString("icerikbaslik"));
//			}
//			
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

	public String[] searchBar(String lookFor) {
		// Metot veri taban�nda yaz� ba�l�klar�nda kelime aramas� yap�yor,
		// aranan
		// kelimeyi al�yor(String lookFor), i�eren ba�l�klar� array(String[]
		// found) olarak d�nd�r�yor.
		String found[] = { "kitap se�imi detaylar�", "se�imler", "se�im sonu�lar�" };
		return found;
	}

	public void setTheme(String theme) {
		temaDB tema = new temaDB();
		userDB user = new userDB();
		tema.setTemaName(theme);
		tema.setUserName(user.getName());
		tema.updateTema();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ArrayList<User> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}
}
