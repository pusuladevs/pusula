package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sun.xml.internal.fastinfoset.util.StringArray;

public class kategoriDB {

	String kategoriname;
	
	public kategoriDB(String kategoriname) {
		this.kategoriname=kategoriname;
	}
	public kategoriDB() {
	}
	
	
	public void addKat() {
		
		String sql = "INSERT INTO kategoriler (name) VALUES (?)";
		try {
			Connection con = ConnectToDB.getConnect();
			
			PreparedStatement pr = con.prepareStatement(sql);
			
			pr.setString(1,getKategoriname());
			
			pr.execute();
			
			con.close();
			System.out.println("Add Kategori Success");
			
		}catch (SQLException e) {
			System.out.println("Kay�t Ba�ar�s�z :"+e);
			
		}
		
	}
	public void delKat() {
		
		String sql = "DELETE FROM `kategoriler` WHERE `kategoriler`.`name` = ?";
		
		try {
			Connection con = ConnectToDB.getConnect();
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, getKategoriname());
			
			ps.execute();
			
			con.close();
			
		}catch (SQLException e){
			System.out.println(e);
		}

}
	
	public String[] getUserKategori(String userName) {
		String sql = "SELECT `kategoriName` FROM `icerik` WHERE `userName` LIKE ? GROUP BY `kategoriName`";
		String[] categories = {""};
		try {
			Connection con = ConnectToDB.getConnect();
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, userName);
			
			ResultSet rs = ps.executeQuery();
			int index = 0;
			while (rs.next()) {
				categories[index] = rs.getString("kategoriName");
				index++;
			}
			
			return categories;
		}catch(SQLException e) {
			System.out.println("get failed kategori " + e);
			return null;
		}
		
	}
	
	// Getters and Setters
	
	public String getKategoriname() {
		return kategoriname;
	}
	public void setKategoriname(String kategoriname) {
		this.kategoriname = kategoriname;
	}
	
	
}
