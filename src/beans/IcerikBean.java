package beans;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import model.ConnectToDB;
import model.icerikDB;
import model.userDB;


@ManagedBean (name="icerik")
@RequestScoped
public class IcerikBean  {
	private Map<String,Boolean > checked = new HashMap<String, Boolean>();
	String icerikbaslik,icerikyazi;
	private String msg;
	long millis=System.currentTimeMillis();  
	java.sql.Date date=new java.sql.Date(millis); 
	ArrayList<icerikDB> icerikler = new ArrayList<icerikDB>();
	icerikDB icerik = new icerikDB();
	icerikDB icerikk;
	userDB user = new userDB();
	String userName = user.getName();
	
	
	public void setIcerikDB() {
		String catname = "";
		for (String cat : checked.keySet()) {
	        if(cat != null){
	        Boolean value = checked.get(cat );
	        if (value) {
	            catname = cat;
	        }

	        }
	    }
		
		icerikDB icerikk = new icerikDB();
        icerikk.setIcerikbaslik(icerikbaslik);
		icerikk.setIcerikyazi(icerikyazi);
		icerikk.setDate(date);
		icerikk.setKategoriname(catname);
		icerikk.setMsg(msg);
		icerikk.setUserName(userName);
		
		icerikk.addIcerik();
		
	}
	
	public  List<icerikDB> getIcerikDB() {
		icerikler.clear();
		String sql = "SELECT * FROM `icerik` WHERE `userName` LIKE ?";
	
	try {
		Connection con = ConnectToDB.getConnect();
		
		PreparedStatement pr = con.prepareStatement(sql);
		pr.setString(1, user.getName());
		ResultSet rs = pr.executeQuery();
		
		while(rs.next()) {
			icerikler.add(new icerikDB(rs.getString("userName"),rs.getString("icerikbaslik"),rs.getString("icerikyazi"),rs.getString("kategoriname"), date));
		}
		con.close();
	}catch (SQLException e) {
		System.out.println("mod"+e);
	}
	return icerikler;
			
	}
	
	
	public void delIcerikDB(icerikDB icerikk) {
		
		icerik.setIcerikbaslik(icerikk.getIcerikbaslik());
		icerik.delIcerik();
		
	}

	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getIcerikbaslik() {
		return icerikbaslik;
	}

	public void setIcerikbaslik(String icerikbaslik) {
		this.icerikbaslik = icerikbaslik;
	}

	public String getIcerikyazi() {
		return icerikyazi;
	}

	public void setIcerikyazi(String icerikyazi) {
		this.icerikyazi = icerikyazi;
	}

	public Map<String, Boolean> getChecked() {
		return checked;
	}

	public void setChecked(Map<String, Boolean> checked) {
		this.checked = checked;
	}

	public String getIduser() {
		return userName;
	}

	public void setIduser(String iduser) {
		this.userName = iduser;
	}

}