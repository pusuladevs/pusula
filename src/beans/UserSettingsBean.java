package beans;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import model.UserSettings;
import model.userDB;

@ManagedBean(name = "sets")
@SessionScoped
public class UserSettingsBean {
	private boolean visibility = true;
	private String userName, email, blogName, about, oldPass, newPass;
	
	userDB user = new userDB();
	UserSettings update = new UserSettings(userDB.getName(),userDB.getPassword(),userDB.getMail());

	public void updateUser() throws IOException {
		update.updateVisibility(visibility);
		if (userName != null) {
			if (!userName.equals("")) {
				update.updateUserName(userName);
			}
		}

		if (email != null) {
			if (!email.equals("")) {
				update.updateEmail(email);
			}
		}

		if (blogName != null) {
			if (!blogName.equals("")) {
				update.updateBlogName(blogName);
			}
		}

		if (about != null) {
			if (!about.equals("")) {
				update.updateAbout(about);
			}
		}

		if (newPass != null) {
			if (!newPass.equals("")) {
				if (!newPass.equals(oldPass)) {
					update.updatePassword(newPass);
				}
			}

			else {
//				addMessage("Eski �ifre yeni �ifre ile ayn� olamaz!");
			}
		}
		 ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		 ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
		 this.about = null;
		 this.blogName = null;
		 this.email = null;
		 this.newPass = null;
		 this.oldPass = null;
		 this.userName = null;
	}

	public void delAccount() {
		update.delAccount();
	}

	public void delBlog() {
		update.delBlog();
	}

	public void checkboxVal() {
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}

	public boolean getVisibility() {
		return this.visibility;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBlogName() {
		return blogName;
	}

	public void setBlogName(String blogName) {
		this.blogName = blogName;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getOldPass() {
		return oldPass;
	}

	public void setOldPass(String oldPass) {
		this.oldPass = oldPass;
	}

	public String getNewPass() {
		return newPass;
	}

	public void setNewPass(String newPass) {
		this.newPass = newPass;
	}

}
