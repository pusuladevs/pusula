package beans;

import java.io.Serializable;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import managers.UriManager;
import model.kategoriDB;
import model.temaDB;

@ManagedBean(name = "uri")
@ApplicationScoped
public class UriTracker implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3507912119508203640L;
	UriManager uriManager = new UriManager();
	private boolean fetched;
	private String userName;

	public String fetchSuccess() {
		userName = UriManager.userName;
//		icerikDB icerik = new icerikDB();
		temaDB tema = new temaDB();
//		
//		userDB about = new userDB();
//		int size = icerik.getContent("q").size();
//		
//		kategoriDB kategori = new kategoriDB();
//		
//		System.out.println("userName UriTracker: " + userName);
//		
//		System.out.println("Tema : " + tema.userTema(userName));
		
		
		return tema.userTema(userName);
	}
	
	public boolean isFetched() {
		return fetched;
	}

	public void setFetched(boolean fetched) {
		this.fetched = fetched;
	}
}
