package beans;

import java.sql.Date;
import java.time.LocalDate;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import model.userDB;

@ManagedBean (name="kayitol")
@RequestScoped
public class KayitOlBean {
	String uyekuladi;
	String uyesifre;
	String uyemail;
	String uyeyetki="user";
	String sonuc="";
	public void uyekayitDB(){
		
			LocalDate datee = LocalDate.now();
			Date date = Date.valueOf(datee);
			
			userDB user = new userDB(this.uyekuladi, this.uyesifre, this.uyemail,this.uyeyetki,date);
			
			user.addUser();
			setSonuc(user.msg);
    }
	
	
	public void uyelerigosterDB(){
		//TODO :
	}

	public String getUyekuladi() {
		return uyekuladi;
	}

	public void setUyekuladi(String uyekuladi) {
		this.uyekuladi = uyekuladi;
	}

	public String getUyesifre() {
		return uyesifre;
	}

	public void setUyesifre(String uyesifre) {
		this.uyesifre = uyesifre;
	}

	public String getUyemail() {
		return uyemail;
	}

	public void setUyemail(String uyemail) {
		this.uyemail = uyemail;
	}

	public String getUyeyetki() {
		return uyeyetki;
	}

	public void setUyeyetki(String uyeyetki) {
		this.uyeyetki = uyeyetki;
	}


	public String getSonuc() {
		return sonuc;
	}


	public void setSonuc(String sonuc) {
		this.sonuc = sonuc;
	}
	
	  public void saveMessage() {
	        FacesContext context = FacesContext.getCurrentInstance();
	         
	        context.addMessage(null, new FacesMessage(sonuc) );
	    }
	
	
}
