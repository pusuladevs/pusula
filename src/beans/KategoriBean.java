package beans;

import javax.faces.bean.SessionScoped;

import model.ConnectToDB;
import model.kategoriDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
@ManagedBean(name="kbean")
@SessionScoped
public class KategoriBean {
	
	private String catName;
	kategoriDB kategorii;
	ArrayList<kategoriDB> kategoriler = new ArrayList<kategoriDB>();
	kategoriDB kategori = new kategoriDB();
	
	public void setKategoriDB(){
	
		kategoriDB kategori =new kategoriDB(catName);
		kategori.addKat();
		

		
	}
	
	public List<kategoriDB> getKategoriDB(){
		kategoriler.clear();
		String sql = "SELECT * FROM `kategoriler`";
	
	try {
		Connection con = ConnectToDB.getConnect();
		
		
		ResultSet rs = con.createStatement().executeQuery(sql);
		
		while(rs.next()) {
			kategoriler.add(new kategoriDB(rs.getString("name")));
		}
		con.close();
	}catch (SQLException e) {
		System.out.println(e);
	}
	return kategoriler;
	}
	
	public void delKategoriDB(kategoriDB kategorii) {
	
		kategori.setKategoriname(kategorii.getKategoriname());
		kategori.delKat();
		
	}	
	
	//Getters and Setters
	
	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public kategoriDB getKategorii() {
		return kategorii;
	}

	public void setKategorii(kategoriDB kategorii) {
		this.kategorii = kategorii;
	}


	
	

} 