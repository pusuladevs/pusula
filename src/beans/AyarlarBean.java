package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import model.userDB;

@ManagedBean (name = "ayarlar")
@RequestScoped
public class AyarlarBean {

	String name,newname,pass,newpass;
	userDB user = new userDB();
	public void editName() {
		//TODO: kullanici adinin editlendigi yer.
		user.editUserName(newname, userDB.getName());
	}
	public void editPass() {
		//TODO: Sifrenin editlendigi yer.
		user.editUserPassword(newpass);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNewname() {
		return newname;
	}
	public void setNewname(String newname) {
		this.newname = newname;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getNewpass() {
		return newpass;
	}
	public void setNewpass(String newpass) {
		this.newpass = newpass;
	}
}
