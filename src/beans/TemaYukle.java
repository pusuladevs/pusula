package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.Part;

@ManagedBean
@SessionScoped
public class TemaYukle {

	Part file;

	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}
	
	//Se�ilen dosyay� bu metot ile y�kl�yoruz.
	public void yukleDB() {
		
		try {
			file.write("C:\\Users\\Furkan\\Desktop\\"+getfilename(file));
			
		} catch (Exception e) {
			System.out.println("Bir hata meydana geldi hata:"+e);
		}
	}
	
	public String getfilename(Part file) {
		
		for(String cd:file.getHeader("content-disposition").split(";"))
            if(cd.trim().startsWith("filename")){
                String filename=cd.substring(cd.indexOf('=')+1).trim().replace("\"", "");
                return filename;
            }
        return "";
	}
	
}
