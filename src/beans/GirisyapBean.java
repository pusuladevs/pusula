package beans;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import managers.SessionManager;
import managers.UriManager;
import model.userDB;

@ManagedBean(name = "girisyap")
@SessionScoped
public class GirisyapBean implements Serializable {

//Bu b�l�mdeki kulid,kuladi,sifre,yetki veri taban�ndan �ekilecek. Yetki t�r�ne g�re y�nlendirme yap�lacak.
//TODO: tmpkuladi ve tmpsifre kullan�c�dan ald���m�z veriler olup kontrolDb metodunda kar��la�t�r�lacak.

	
	private static final long serialVersionUID = 2897894540358646965L;
	private String kulid;// Veritaban�ndaki Kullan�c�n�n id si.
	private String yetki; // Veritaban�nda kay�tl� olan kullan�c� yetkisi.
	private String tmpkuladi;// Giri� ekran�nda ald���m�z input de�eri.
	private String tmpsifre;// Giri� ekran�nda ald���m�z input de�eri.
	private UriManager uriManager = new UriManager();

	public void redirect() throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect("http://localhost:8080/pusula/blogger/"+tmpkuladi+".xhtml");
    }
	public String kontrolDB() {
		userDB User = new userDB();

		boolean loginStatus = User.loginUser(tmpkuladi, tmpsifre);

		if (loginStatus) {
			this.yetki = User.getYetki();
			return (redirectUser(User.getYetki())).toString();
		} else {
			addMessage("Hatal� giri� yap�ld�!");
			return "";
		}
	}

	public String redirectUser(String auth) {
		HttpSession session = SessionManager.getSession();
		session.setAttribute("auth", auth);

		return auth;
	}

	public String logout() {
		HttpSession session = SessionManager.getSession();
		if (session != null && session.getAttribute("auth") != null) {
			session.invalidate();
		}
		return "loginpage";
	}

	public void addMessage(String message) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, message, null);
		FacesContext.getCurrentInstance().addMessage("loginMsg", msg);
	}

	// Getters and Setters

	public String getTmpkuladi() {
		return tmpkuladi;
	}

	public void setTmpkuladi(String tmpkuladi) {
		this.tmpkuladi = tmpkuladi;
	}

	public String getTmpsifre() {
		return tmpsifre;
	}

	public void setTmpsifre(String tmpsifre) {
		this.tmpsifre = tmpsifre;
	}

	public String getYetki() {
		return yetki;
	}

	public void setYetki(String yetki) {
		this.yetki = yetki;
	}

	public String getKulid() {
		return kulid;
	}

	public void setKulid(String kulid) {
		this.kulid = kulid;
	}

}