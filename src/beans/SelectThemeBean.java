package beans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import model.UserBlog;

@ManagedBean(name = "selectTheme")
@SessionScoped
public class SelectThemeBean {
	private String selectedTheme;

	public void setThemeToBlog() {
		UserBlog user = new UserBlog();
		user.setTheme(selectedTheme);
		
	}
	
	public void getVal(ActionEvent event) {
		selectedTheme = (String)event.getComponent().getAttributes().get("val");
		
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Tema blo�unuza uyguland�!" , null);
		FacesContext.getCurrentInstance().addMessage("msgFromSelectTheme", message);
	}
	
	public String getSelectedTheme() {
		return selectedTheme;
	}

	public void setSelectedTheme(String selectedTheme) {
		this.selectedTheme = selectedTheme;
	}
	
	
}
