package beans;

import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.tagcloud.DefaultTagCloudItem;
import org.primefaces.model.tagcloud.DefaultTagCloudModel;
import org.primefaces.model.tagcloud.TagCloudItem;
import org.primefaces.model.tagcloud.TagCloudModel;
import entities.Contents;
import entities.User;
import managers.UriManager;
import model.UserBlog;
import model.UserBlog.CategoriesWithHeaders;

@ManagedBean(name = "userTheme")
@RequestScoped
public class UserThemeBean {

	private boolean loaded;
	UserBlog userBlog = new UserBlog();
	private User user;
	private Contents selectedContent;
	private TagCloudModel categories;
	private ArrayList<String> categoriesArray = new ArrayList<String>();
	private String searchFor;
	private ArrayList<String> headers = new ArrayList<String>();
	private ArrayList<CategoriesWithHeaders> categoriesWithHeaders = new ArrayList<UserBlog.CategoriesWithHeaders>();


	@PostConstruct
	public void init() {
		
		this.user = fetchUsers();
		this.categoriesArray = userBlog.fetchCategories(UriManager.userName);
		categoriesForCloud(userBlog.fetchCategories(UriManager.userName));
		CategoriesWithHeaders defaultHeaders = userBlog.fetchCategoriesWithHeaders(categoriesArray.get(0), UriManager.userName);
		ArrayList<String> headers = defaultHeaders.getHeaders();
		
		for(String header: headers) {
			this.headers.add(header);
		}
		
		for(int i=0; i<categoriesArray.size(); i++) {
			categoriesWithHeaders.add(userBlog.fetchCategoriesWithHeaders(categoriesArray.get(i), UriManager.userName));
		}
		
		selectedContent = userBlog.fetchContent(this.headers.get(0), UriManager.userName);
	}
	
	public User fetchUsers() {
		User user;
		user = userBlog.fetchUser(UriManager.userName);
		
		return user;
	}
	
	public void onHeaderSelect(String header) {
//		this.categoriesWithHeaders.clear();
//		for(int i=0; i<categoriesArray.size(); i++) {
//			categoriesWithHeaders.add(userBlog.fetchCategoriesWithHeaders(categoriesArray.get(i), UriManager.userName));
//		}
		System.out.println("header: " + header);
		this.selectedContent = userBlog.fetchContent(header, UriManager.userName );
		System.out.println("selectedContent: " + this.selectedContent.getHeader());
	}

	public void categoriesForCloud(ArrayList<String> categoriesArray) {
		categories = new DefaultTagCloudModel();

		for (String category : categoriesArray) {
			categories.addTag(new DefaultTagCloudItem(category, randomGenerator(5)));
		}
	}

	private int randomGenerator(int max) {
		int rand = (int) (Math.random() * max);
		return rand;
	}

	// Arama butonu
//	public void buttonSearch() {
//
//		if (this.searchFor == null) {
//			addMessage("Arama kelimesi girin...");
//		}
//
//		else if (this.searchFor.length() <= 2) {
//			addMessage("Arama yapabilmek i�in en az 3 karakter girmelisiniz.");
//		}
//
//		else {
//			// themeGame
//			this.headers.clear();
//			for (int i = 0; i < userBlog.searchBar(searchFor).length; i++) {
//				this.headers.add(userBlog.searchBar(searchFor)[i]);
//			}
//			// themeFruits
//			this.categoriesWithHeaders.clear();
//			this.categoriesWithHeaders.add(userBlog.new CategoriesWithHeaders("", userBlog.searchBar(searchFor)));
//		}
//	}

	// Arama alan� mesajlar�
	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
		FacesContext.getCurrentInstance().addMessage("msgFromBlog", message);
	}


	public void onCategorySelect(SelectEvent<TagCloudItem> event) {
		TagCloudItem item = event.getObject();
		String selectedCategory = item.getLabel();
		CategoriesWithHeaders tmp = userBlog.fetchCategoriesWithHeaders(selectedCategory, UriManager.userName);

		this.headers.clear();
		for(int i=0; i<tmp.getHeaders().size(); i++) {
			headers.add(tmp.getHeaders().get(i));
		}
	}

	

	public User getuser() {
		return user;
	}

	public void setuser(User user) {
		this.user = user;
	}

	public String getSearchFor() {
		return searchFor;
	}

	public void setSearchFor(String searchFor) {
		this.searchFor = searchFor;
	}

	public Contents getSelectedContent() {
		return selectedContent;
	}

	public void setSelectedContent(Contents selectedContent) {
		this.selectedContent = selectedContent;
	}

	public TagCloudModel getCategories() {
		return categories;
	}

	public ArrayList<String> getHeaders() {
		return headers;
	}

	public void setHeaders(ArrayList<String> headers) {
		this.headers = headers;
	}

	public ArrayList<String> getCategoriesArray() {
		return categoriesArray;
	}

	public void setCategoriesArray(ArrayList<String> categoriesArray) {
		this.categoriesArray = categoriesArray;
	}

	public ArrayList<CategoriesWithHeaders> getCategoriesWithHeaders() {
		return categoriesWithHeaders;
	}

	public void setCategoriesWithHeaders(ArrayList<CategoriesWithHeaders> categoriesWithHeaders) {
		this.categoriesWithHeaders = categoriesWithHeaders;
	}
	
	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}
}
