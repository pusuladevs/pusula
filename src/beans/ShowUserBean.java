package beans;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import model.ConnectToDB;
import model.userDB;

@ManagedBean (name = "user")
@SessionScoped
public class ShowUserBean {

	ArrayList<userDB> users = new ArrayList<userDB>();
	userDB user = new userDB();
	
	
	public List<userDB> getUser(){
		
		users.clear();
			String sql = "SELECT * FROM `users` WHERE `yetki` LIKE 'user' ORDER BY `name` DESC";
		
		try {
			Connection con = ConnectToDB.getConnect();
			
			
			ResultSet rs = con.createStatement().executeQuery(sql);
			
			while(rs.next()) {
				users.add(new userDB(rs.getString("name"),null,null,rs.getString("yetki"),rs.getDate("tarih")));
			}
			con.close();
		}catch (SQLException e) {
			System.out.println("mod"+e);
		}
		return users;
		
			
	}
	public void redirect(String name) throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect("http://localhost:8080/pusula/blogger/"+name+".xhtml");
    }
public void delUser(userDB userr) {
        
      
	 user.setName(userr.uname);
        
     user.delUser();
        
    }
	
	
}
