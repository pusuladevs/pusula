package beans;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import model.ConnectToDB;
import model.userDB;
import entities.User;

@ManagedBean (name="modekle")
@SessionScoped
public class ModBean {
	private String modKulAdi;
	private String modMail;
	private String modSifre;
	private String yetki="mod";
	private userDB userr;
	private String sonuc="";
	

	ArrayList<userDB> modlar = new ArrayList<userDB>();

	userDB user = new userDB();
	public List<User> setModDB(){
		LocalDate datee = LocalDate.now();
		Date date = Date.valueOf(datee);
		
		userDB user = new userDB(this.modKulAdi, this.modSifre, this.modMail,this.yetki,date);
		
		user.addUser();
		setSonuc(user.msg);
		
		return null;
	}
	
	public List<userDB> getModDB(){
		
		modlar.clear();
			String sql = "SELECT * FROM `users` WHERE `yetki` LIKE 'mod' ORDER BY `yetki` DESC";
		
		try {
			Connection con = ConnectToDB.getConnect();
			
			
			ResultSet rs = con.createStatement().executeQuery(sql);
			
			while(rs.next()) {
				modlar.add(new userDB(rs.getString("name"),rs.getString("password"),rs.getString("mail"),
						rs.getString("yetki"),rs.getDate("tarih")));
			}
			
			for(int i=0; i<modlar.size(); i++) {
				System.out.println(modlar.get(i).yetki);
			}
			con.close();
		}catch (SQLException e) {
			System.out.println("mod"+e);
		}
		return modlar;
		
			
	}
	
	public void delModDB(userDB userr) {
        
        user.setName(userr.uname);
        
        user.delUser();
        
    }
	
	//Getters and Setters


	public String getModKulAdi() {
		return modKulAdi;
	}

	public void setModKulAdi(String modKulAdi) {
		this.modKulAdi = modKulAdi;
	}

	public String getModMail() {
		return modMail;
	}

	public void setModMail(String modMail) {
		this.modMail = modMail;
	}

	public String getModSifre() {
		return modSifre;
	}

	public void setModSifre(String modSifre) {
		this.modSifre = modSifre;
	}

	public String getYetki() {
		return yetki;
	}

	public void setYetki(String yetki) {
		this.yetki = yetki;
	}

	public String getSonuc() {
		return sonuc;
	}

	public void setSonuc(String sonuc) {
		this.sonuc = sonuc;
	}


	public userDB getUser() {
		return user;
	}

	public void setUser(userDB user) {
		this.user = user;
	}

	public userDB getUserr() {
		return userr;
	}

	public void setUserr(userDB userr) {
		this.userr = userr;
	}

	
	
}
